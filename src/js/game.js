"use strict";

var ajax = new XMLHttpRequest();
var stats = {
    set(JSONInput) {
        var arr = JSON.parse(JSONInput);
        var statSheet = document.querySelector(".container").querySelectorAll("span.row");
        var unspent = document.getElementById("charSheet").querySelector("span").children[1];
        var loop = 0;

        for (var obj in arr) {
            if (loop == statSheet.length) {
                unspent.value = unspent.max = arr[obj];
            } else {
                statSheet[loop].children[0].innerHTML = obj.charAt(0).toUpperCase() + obj.slice(1) + ":";
                statSheet[loop].children[1].value = statSheet[loop].children[1].min = arr[obj];
                loop++;
            }
        }
    },
    events() {
        var unspent = document.getElementById("charSheet").querySelector("span").children[1];
        window.modified = 0;
        document.querySelectorAll(".btn-group").forEach(function (grp) {
            grp.children[0].addEventListener("click", function (e) {
                var stat = e.target.parentNode.previousElementSibling.lastElementChild;

                if (Number(stat.value) == Number(stat.min) && Number(unspent.value) > 0) {
                    window.modified += 1;
                }

                if (unspent.value > 0) {
                    stat.stepUp(1);
                    unspent.stepDown(1);
                }

                if (modified > 0) {
                    document.getElementById("save").disabled = false;
                }
            });
            grp.children[1].addEventListener("click", function (e) {
                var stat = e.target.parentNode.previousElementSibling.lastElementChild;

                if (Number(stat.value) == Number(stat.min) + 1) {
                    window.modified -= 1;
                }

                if (stat.value > stat.min) {
                    stat.stepDown(1);
                    unspent.stepUp(1);
                }

                if (modified == 0) {
                    document.getElementById("save").disabled = true;
                }
            });
        });
    },
    save() {
        var json = {};
        document.querySelectorAll("input[type=number]").forEach(function (el) {
            var objKey = el.previousElementSibling.innerHTML.toLowerCase().replace(":", "").replace("available stat points", "unspent");
            var objVal = el.value;
            json[objKey] = Number(objVal);
        });
        var btn = document.getElementById("save");
        btn.disabled = true;
        ajax.open("POST", "src/php/charFunc.php");
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statIncrease=" + JSON.stringify(json));

        ajax.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                btn.innerHTML = "Save changes";
            } else if (this.readyState != 4) {
                btn.innerHTML = "Saving...";
            }
        };

        return JSON.stringify(json);
    },
};
document.getElementById("save").addEventListener("click", stats.save);

function ajaxPage(header, data) {
    header == "fetch" ? (header = "choice") : (header = "page");
    ajax.open("POST", "src/php/pageFunc.php");
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send(header + "=" + data);

    ajax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var result = JSON.parse(this.responseText);
            document.getElementById("text").innerHTML = result[0];
            document.getElementById("buttons").innerHTML = result[1];
            stats.set(result[2]);
            document.getElementById("pageContent").style.opacity = 1;
            page.events();
        }
    };
}

var page = {
    get() {
        ajaxPage("get", null);
    },
    fetch(i) {
        document.getElementById("pageContent").style.opacity = 0;
        setTimeout(function () {
            ajaxPage("fetch", i);
        }, 250);
    },
    events() {
        document
            .getElementById("buttons")
            .querySelectorAll(".btn")
            .forEach(function (btn) {
                btn.addEventListener("click", function () {
                    var par = btn.parentNode;
                    var i = Array.prototype.indexOf.call(par.children, btn);
                    page.fetch(i);
                });
            });
    },
};
document.querySelector(".navbar").lastElementChild.addEventListener("click", function (e) {
    var page = document.getElementById("pageContent").style;
    var sheet = document.getElementById("charSheet").style;

    if (page.display == "none") {
        e.target.innerHTML = "Character sheet";
        page.display = "flex";
        sheet.display = "none";
    } else {
        e.target.innerHTML = "Back to the story";
        page.display = "none";
        sheet.display = "flex";
    }
});

function finish() {
    var nav = document.querySelector("div .navbar").style;
    var loader = document.getElementById("loader").style;
    var content = document.querySelector("div .content").style;
    nav.opacity = 1;
    nav.transform = "translateY(0)";
    loader.opacity = 0;
    loader.visibility = "hidden";
    content.opacity = 1;
    content.transform = "translateY(0)";
    content.visibility = "visible";
}

(document.getElementById("text").onload = finish()), page.get(), stats.events();
