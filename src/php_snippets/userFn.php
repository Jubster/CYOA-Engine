<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/src/php/encryptor.php';
$module_root = $_SERVER["DOCUMENT_ROOT"].'/src/html_modules';
if (isset($_COOKIE['userData'])) {
    include $module_root.'/logged.html';
} else {
    include $module_root.'/login.html';
}
if (isset($_COOKIE['userData']) && json_decode(safeDecrypt($_COOKIE['userData'],$key),true)[2] == 100) {
    include $module_root.'/admin.html';
}
?>