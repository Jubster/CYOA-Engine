<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/src/php/encryptor.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/src/php/gameData.php';
session_start();
$pageRoot = $_SERVER["DOCUMENT_ROOT"].'/pages';
if(!isset($_SESSION['gameState'])){$_SESSION['gameState'] = 'normal';}
    if (isset($_POST['page'])){
        $pageFile = file_get_contents($pageRoot.'/chapter_'.$decrypted['chapter'].'/'.$decrypted['page'].'.html');
        $jsonFile = json_decode(file_get_contents($pageRoot.'/chapter_'.$decrypted['chapter'].'/'.$decrypted['page'].'.json'),true);
        echo getPage($pageFile,$jsonFile,$decrypted['page']);
    }
    if (isset($_POST['choice'])) {
        $index = $_POST['choice'];
        $pageFile = file_get_contents($pageRoot.'/'.$_SESSION['links'][$index].'.html');
        $jsonFile = json_decode(file_get_contents($pageRoot.'/'.$_SESSION['links'][$index].'.json'),true);
        echo getPage($pageFile,$jsonFile,$_SESSION['links'][$index]);
    }
    function getPage($file,$json,$filename) {
        global $decrypted,$key,$time;
        $_SESSION['links'] = array();
        $returnButtons = "";
        $fail = 0;
        if (count($json['statProperties']['stats']) != 0){
            foreach ($json['statProperties']['stats'] as $x) {
                if ($x['value'] > $decrypted['stats'][$x['stat']]){
                    $fail = 1;
                    //While it would be nice to allow for separate failures, with separate fail texts for each, it isn't feasible.
                    //Like, if you succeed strength but fail constitution, holding up a falling roof; can do it, but not long enough
                }
            }
        }
        $fail == 0 ? $returnText = $file : $returnText = $json['statProperties']['txt'];
        if ($json['statProperties']['gameOver'] == true && $fail == 1) {
            $_SESSION['gameState'] = 'gameover';
            $returnButtons .= '<button class="btn btn-custom col-4 square">Return to checkpoint</button>';
            $returnButtons .= '<button class="btn btn-custom col-4 square">Start from the beginning</button>';
            array_push($_SESSION['links'],$decrypted['page'],'intro');
        } else {
            foreach ($json['choices'] as $a) {
                if ($a['link'] != "") {
                    $returnButtons .= '<button class="btn btn-custom col-4 square">'.$a['choice'].'</button>';
                    array_push($_SESSION['links'],"chapter_".$a['chapter'].'/'.$a['link']);
                }
            }
        }
        if ($json['checkpoint'] == true) {
            ($decrypted['page'] != basename($filename))?$decrypted['checkpointStats'] = $decrypted['stats']:$decrypted['stats'] = $decrypted['checkpointStats'];
            $decrypted['page'] = basename($filename);
            $modified = true;
        }
        if ($json['statProperties']['points'] > 0) {
            $decrypted['stats']['unspent'] += $json['statProperties']['points'];
            $modified = true;
        }
        if ($returnButtons == "") {
            $returnText .= "<h4 class='col-12 mt-5'>Error: Page was not found. Tell the writer to set links to this page, they'll know what that means</h4>";
        }
        if(isset($modified)){setcookie('gameData',safeEncrypt(json_encode($decrypted),$key),$time);}
        return json_encode(array($returnText,$returnButtons,json_encode($decrypted['stats']),$file,$json,$filename,json_encode($_SESSION['links'])));
    }
?>