<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/src/php/encryptor.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/src/php/gameData.php';
function cheatCheck($input) {
    global $decrypted;
    $flag = true;
    $oldTotal = 0;
    $newTotal = 0;
    foreach ($decrypted['stats'] as $n) {
        $oldTotal += $n;
    }
    foreach ($input as $n ) {
        $newTotal += $n;
    }
    if ($oldTotal != $newTotal) {
        $flag = false;
    }
    if ($flag) {
        foreach ($input as $stat => $val) {
            if ($decrypted['stats'][$stat] > $val && $stat != 'unspent') {
                $flag = false;
            }
        }
    }
    return $flag;
}
function statIncrease($input) {
    global $decrypted;
    if(cheatCheck($input)) {
        foreach ($input as $stat => $val) {
                $decrypted['stats'][$stat] = $val;
            }
    };
}
if (isset($_POST['statIncrease'])) {
    statIncrease(json_decode($_POST['statIncrease'],true));
}
setcookie('gameData',safeEncrypt(json_encode($decrypted),$key),$time);
?>