async function dbPromise() {
    return idb.openDB("pageIndex", 1, {
        upgrade(db) {
            const store = db.createObjectStore("pages", {});
        },
    });
}
const db = {
    async set(ch, page) {
        return (await dbPromise()).put("pages", [ch, page], ch + "-" + page);
    },
    async delete(ch, page) {
        return (await dbPromise()).delete("pages", ch + "-" + page);
    },
    async get(key) {
        return (await dbPromise()).get("pages", key);
    },
    async keys() {
        return (await dbPromise()).getAllKeys("pages");
    },
};
