"use strict";

/*
    createElement/appendChild method is a LOOOOOOOT more code, but in testing it had more performance that innerHTML +=, so ok. 
    I can suffer for 50ms faster loads (When adding all stat checks, so individually around 8ms per add).
    All of that is probably lost when loading the page, but I personally prefer frontloading as much as possible, so that actual use is fast anyway.
*/
document.querySelector("#checkAdd").addEventListener("change", addStatCheck);

function addStatCheck(event) {
    if (event.type == "change" || event == "manualTrigger") {
        var statSel = document.getElementById("checkAdd");
        var selectedStat = statSel.options[statSel.selectedIndex];
        var list = document.getElementById("checkList");
        var divRow = document.createElement("div");
        var div1 = document.createElement("div");
        var div2 = document.createElement("div");
        var label = document.createElement("label");
        var input = document.createElement("input");
        var button = document.createElement("button");
        var img = document.createElement("img");
        divRow.classList.add("row", "no-gutters", "mb-1", "justify-content-start");
        div1.classList.add("col-12");
        div2.classList.add("col-9");
        div2.classList.add("col-auto");
        label.innerHTML = selectedStat.text;
        label.className = "mb-0";
        input.className = "form-control";
        input.type = "number";
        input.min = 1;
        input.max = 10;
        input.value = 1;
        button.classList.add("col-2", "btn", "btn-danger", "ml-auto", "trash");
        img.src = "../resources/trash.svg";
        img.width = 16;
        img.height = 16;
        divRow.appendChild(div1);
        divRow.appendChild(div2);
        divRow.appendChild(button);
        div1.appendChild(label);
        div2.appendChild(input);
        button.appendChild(img);

        if (list.getElementsByTagName("small").length == 1) {
            list.innerHTML = "";
        }

        list.appendChild(divRow);
        document.getElementById("statFail").style.display = "block";
        selectedStat.style.display = "none";
        selectedStat.disabled = true;
        statSel.value = 0;

        if (list.childElementCount == statSel.length - 1) {
            statSel.disabled = true;
        }

        button.addEventListener("click", function () {
            this.parentNode.remove();
            selectedStat.style.display = "block";
            selectedStat.disabled = false;
            statSel.disabled = false;

            if (list.childElementCount == 0) {
                document.getElementById("statFail").style.display = "none";
            }
        });
    }
}

document.querySelector("#choiceAdd").addEventListener("click", function () {
    var list = document.getElementById("choiceList");
    var div = document.createElement("div");
    var div2 = document.createElement("div");
    var input = document.createElement("input");
    var button = document.createElement("button");
    var img = document.createElement("img");
    div.classList.add("no-gutters", "row", "align-items-center", "mb-1", "justify-content-between");
    div2.classList.add("col-8", "col-md-10");
    input.type = "text";
    input.placeholder = "Write text here";
    input.classList.add("form-control");
    button.classList.add("col-2", "col-md-1", "btn", "btn-danger", "trash");
    img.src = "../resources/trash.svg";
    img.width = 16;
    img.height = 16;
    div.appendChild(div2);
    div2.appendChild(input);
    div.appendChild(button);
    button.appendChild(img);
    list.previousElementSibling.style.display = "none";
    list.appendChild(div);
    button.addEventListener("click", function () {
        this.parentNode.remove();
        addLink(list.childNodes.length);

        if (list.getElementsByTagName("div").length == 0) {
            list.previousElementSibling.style.display = "block";
        }
    });
    addLink(list.childNodes.length);
});

function addLink(i) {
    var list = document.getElementById("linkList");
    var clone = list.lastElementChild.cloneNode(true);

    if (i >= 2 && i > list.querySelectorAll(".dropdown").length) {
        clone.firstElementChild.value = "";
        list.appendChild(clone);
    } else if (list.querySelectorAll(".dropdown").length > 1) {
        list.lastElementChild.remove();
    }
}

var choices = document.getElementById("choiceList");
var statList = document.getElementById("checkList");
var links = document.getElementById("linkList");
var properties = [choices, statList];

function readProperties(file) {
    file = JSON.parse(file);

    if (file.choices.length > 0) {
        file.choices.forEach(function (i) {
            document.getElementById("choiceAdd").click();
            choices.lastElementChild.firstChild.lastChild.value = i.choice;
            links.lastElementChild.firstElementChild.value = i.link;
        });
    }

    if (file.statProperties.stats.length > 0) {
        file.statProperties.stats.forEach(function (i) {
            document.querySelector("#checkAdd").value = i.stat.charAt(0).toUpperCase() + i.stat.slice(1);
            addStatCheck("manualTrigger");
            statList.lastElementChild.getElementsByTagName("div")[1].firstElementChild.value = i.value;
        });
    }

    document.getElementById("checkpoint").checked = file.checkpoint;
    statFail.childNodes[3].childNodes[1].checked = file.statProperties.gameOver;
    editors.failText.setData(file.statProperties.txt);

    if (file.statProperties.points > 0) {
        document.getElementById("statIncrease").value = file.statProperties.points;
    }
}

function getProperties() {
    var json = {
        title: document.getElementById("title").value,
        choices: [],
        statProperties: {
            stats: [],
            gameOver: "",
            txt: "",
        },
        checkpoint: "",
    };
    properties.forEach(function (i) {
        var obj = {};
        json.checkpoint = document.getElementById("checkpoint").checked;

        switch (i) {
            case choices:
                try {
                    if (i.childNodes.length > 0) {
                        var loop = 0;
                        i.childNodes.forEach(function (x) {
                            var choice = x.childNodes[0].childNodes[0].value != "" ? x.childNodes[0].childNodes[0].value : null;
                            obj = {
                                choice: choice,
                                link: links.querySelectorAll(".dropdown-toggle")[loop].value.replace(/\s/g,"_").toLowerCase(),
                                chapter: document.getElementById("chSel").value, // TODO: This should be set in the link, but for testing purposes, I'm using this
                            };

                            if (choice != null) {
                                json.choices.push(obj);
                            }

                            loop++;
                        });
                    } else {
                        obj = {
                            choice: "Continue",
                            link: links.querySelectorAll(".dropdown-toggle")[0].value,
                            chapter: document.getElementById("chSel").value, // TODO: This should be set in the link, but for testing purposes, I'm using this
                        };
                        json.choices.push(obj);
                    }
                } catch (e) {}

                break;

            case statList:
                document.getElementById("statIncrease").value > 0
                    ? (json.statProperties.points = Number(document.getElementById("statIncrease").value))
                    : (json.statProperties.points = 0);

                if (i.childElementCount != 0) {
                    try {
                        i.childNodes.forEach(function (x) {
                            obj = {
                                stat: x.childNodes[0].childNodes[0].innerHTML.toLowerCase(),
                                value: Number(x.childNodes[1].childNodes[0].value),
                            };
                            json.statProperties.stats.push(obj);
                        });
                        json.statProperties.gameOver = statFail.childNodes[3].childNodes[1].checked;
                        json.statProperties.txt = encodeURIComponent(editors.failText.getData());
                    } catch (e) {}
                }

                break;

            default:
                break;
        }
    });
    return JSON.stringify(json);
}

function clearProperties(save) {
    document.getElementById("statIncrease").value = "";
    document
        .getElementById("pgProperties")
        .querySelectorAll(".btn-danger")
        .forEach(function (i) {
            return i.click();
        });
    links.querySelectorAll(".dropdown-toggle").forEach(function (i) {
        i.value = "";
    });
    Object.keys(editors).forEach(function (i) {
        editors[i].setData("");
    });
    document.getElementById("checkpoint").checked = false;
    title.value = "";

    if (save) {
        document.getElementById("filter").value = "";
    }
}
