"use strict";

document.getElementById("filter").onchange = function () {
    var title = document.getElementById("title");
    var curPage = document.getElementById("filter").value;
    var elToToggle = [
        title.disabled,
        document.getElementById("submit").disabled,
        document.querySelector('a[href="#pgProperties"]').dataset["disabled"],
        document.querySelector('a[href="#gameProperties"]').dataset["disabled"],
    ];
    clearProperties();

    if (curPage != "New Page") {
        ajax.open("POST", "src/php/editor.php", true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("chapter=" + Number(document.getElementById("chSel").value) + "&q=" + encodeURIComponent(curPage));

        ajax.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText == "404") {
                    title.value = "";
                    editors.mainEditor.setData("File not found");
                    title.disabled = document.getElementById("submit").disabled = document.querySelector('a[href="#pgProperties"]').dataset[
                        "disabled"
                    ] = document.querySelector('a[href="#gameProperties"]').dataset["disabled"] = true;
                } else {
                    title.value = curPage;
                    title.disabled = document.getElementById("submit").disabled = document.querySelector('a[href="#pgProperties"]').dataset[
                        "disabled"
                    ] = document.querySelector('a[href="#gameProperties"]').dataset["disabled"] = false;
                    getProperties(curPage);
                    editors.mainEditor.setData(JSON.parse(this.responseText)[0]);
                    readProperties(JSON.parse(this.responseText)[1]);
                }
            }
        };
    }
    if (curPage == "Intro") {
        title.disabled = true;
    } else {
        title.disabled = false;
    }
};

var populate = {
    pages(el) {
        var pages = [];
        var promiseArray = [];

        db.keys()
            .then((keys) => {
                keys.forEach((key) => {
                    promiseArray.push(
                        db.get(key).then((result) => {
                            pages.push(result);
                        })
                    );
                });
            })
            .then(() => {
                Promise.all(promiseArray).then(() => {
                    el.innerHTML = null;
                    if (pages.length == 0 && document.getElementById("chSel").value == 1) {
                        db.set(1, "Intro");
                        el.innerHTML += '<a class="dropdown-item" href="#" data-show="true">Intro</a>';
                    } else {
                        pages.forEach(function (i) {
                            if (i[0] == document.getElementById("chSel").value) {
                                el.innerHTML += '<a class="dropdown-item" href="#" data-show="true">' + i[1] + "</a>";
                            }
                        });
                    }

                    if (el.id == "mainSel") {
                        el.parentNode.firstElementChild.addEventListener("click", function () {
                            el.parentNode.previousElementSibling.value = "New Page";
                            el.parentNode.previousElementSibling.onchange();
                        });
                    }

                    el.childNodes.forEach(function (x) {
                        x.addEventListener("click", function () {
                            el.parentNode.previousElementSibling.value = x.innerHTML;

                            if (el.id == "mainSel") {
                                el.parentNode.previousElementSibling.onchange();
                            }
                        });
                    });

                    populate.chapters();
                });
            });
    },

    chapters() {
        var chapters = [];
        var promiseArray = [];

        db.keys()
            .then((keys) => {
                keys.forEach((key) => {
                    promiseArray.push(
                        db.get(key).then((result) => {
                            if (!chapters.includes(result[0])) {
                                chapters.push(result[0]);
                            }
                        })
                    );
                });
            })
            .then(() => {
                Promise.all(promiseArray).then(() => {
                    populate.newChapter(chapters.length, false);
                });
            });
    },

    newChapter(chaptersInDb, manual) {
        var el = document.getElementById("chSel");
        if (el.length == chaptersInDb + 1) {
            return;
        }
        for (let i = el.length; el.length < chaptersInDb + 1; i++) {
            var prevEl = el.lastElementChild;
            prevEl.text = "Chapter " + prevEl.value;
            var opt = document.createElement("option");
            opt.value = Number(prevEl.value) + 1;
            opt.text = "Create new chapter";
            el.appendChild(opt);
        }
        if (manual) {
            el.disabled = true;
            document.getElementById("filter").disabled = true;
            document.getElementById("filter").value = "New Page";
        }
    },
};

document.getElementById("selRefresh").addEventListener("click", (e) => {
    e.target.disabled = true;
    document.querySelectorAll("input").forEach((el) => {
        el.disabled = true;
    });
    ajax.open("POST", "src/php/editor.php", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("refresh=true");

    ajax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            var promiseArray = [];

            response.forEach((page) => {
                promiseArray.push(db.set(page.chapter, page.title));
            });

            Promise.allSettled(promiseArray).then(() => {
                e.target.disabled = false;
                document.querySelectorAll("input").forEach((el) => {
                    el.disabled = false;
                });
                prepSel();
            });
        }
    };
});

document.getElementById("chSel").addEventListener("change", (e) => {
    clearProperties(true);
    prepSel();
    if (e.target.options[e.target.selectedIndex].text == "Create new chapter") {
        populate.newChapter(document.getElementById("chSel").length, true);
    }
});

function prepSel() {
    document.querySelectorAll(".dropdown-menu").forEach((i) => {
        populate.pages(i.lastElementChild);
    });

    var filterText = "";
    document.querySelectorAll("input.dropdown-toggle").forEach((item) => {
        item.addEventListener("keydown", function (e) {
            if (e.code == "Tab" || e.code == "Enter") {
                e.preventDefault();

                if (filterText == "" || item.value == "") {
                    return;
                }

                $(item).dropdown("toggle");
                item.value = filterText;
                filterText = "";
                item.blur();
                return;
            }
        });

        item.addEventListener("keyup", (e) => {
            if (e.code == "Tab" || e.code == "Enter") {
                e.preventDefault();
                return;
            }

            var input = item.value.toLowerCase();
            var x = item.nextElementSibling.lastElementChild.childNodes;

            for (var index = x.length - 1; index >= 0; index--) {
                var txt = x[index].textContent.toLowerCase();

                if (txt.indexOf(input) > -1) {
                    x[index].style.display = "block";
                    x[index].setAttribute("data-show", "true");
                } else {
                    x[index].style.display = "none";
                    x[index].setAttribute("data-show", "false");
                }
            }

            if (item.nextElementSibling.lastElementChild.childElementCount > 0 && item.id == "filter") {
                try {
                    filterText = item.nextElementSibling.lastElementChild.querySelector("[data-show='true']").text;
                    item.nextElementSibling.querySelector(".dropdown-divider").style.display = "block";
                } catch (error) {
                    filterText = "";
                    item.nextElementSibling.querySelector(".dropdown-divider").style.display = "none";
                }
            } else if (item.nextElementSibling.lastElementChild.childElementCount > 0) {
                try {
                    filterText = item.nextElementSibling.lastElementChild.querySelector("[data-show='true']").text;
                } catch (error) {
                    filterText = "";
                }
            }
        });
    });
}

document.body.onload = prepSel();
