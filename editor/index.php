<?php 
    require $_SERVER["DOCUMENT_ROOT"].'/src/php/encryptor.php';
    if (!isset($_COOKIE['userData']) || (isset($_COOKIE['userData']) && json_decode(safeDecrypt($_COOKIE['userData'],$key),true)[2] != 100)) {
        header('location:../');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Roolipeli moottori</title>
    <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
    <div class="mx-auto col-10 main py-3">
        <h4>Page search</h4>
        <div class="row no-gutters">
            <select id="chSel" class="form-control col-2">
                <option value="1">Chapter 1</option>
                <option value="2">Create new chapter</option>
            </select>
            <div class="dropdown col-12 col-lg px-lg-3">
                <input type="text" class="form-control dropdown-toggle" id="filter" placeholder="Search for a page"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="dropdown-menu col">
                    <a class="dropdown-item" href="#">New Page</a>
                    <div class="dropdown-divider"></div>
                    <div id="mainSel"></div>
                </div>
            </div>
            <button class="btn btn-custom" id="selRefresh"><img src="../resources/arrow-clockwise.svg"
                    alt="Refresh page selector"></button>
        </div>
    </div>
    <div id="editWrapper" class="mx-auto col-10 main p-0">
        <ul class="nav nav-tabs mb-3" id="tabList" role="tablist">
            <li class="nav-item"><a href="#text" class="nav-link active" role="tab" data-toggle="tab">Text
                    editor</a></li>
            <li class="nav-item"><a href="#pgProperties" class="nav-link" role="tab" data-toggle="tab">Page
                    Properties</a></li>
            <li class="nav-item"><a href="#gameProperties" class="nav-link" role="tab" data-toggle="tab">Game
                    Properties</a></li>
        </ul>
        <div class="tab-content px-3" id="tabContent">
            <div id="text" class="tab-pane fade show active" role="tabpanel">
                <div class="form-group">
                    <label for="title">Page title</label>
                    <input type="text" id="title" class="form-control" require>
                </div>
                <div class="form-group">
                    <label for="mainEditor">Page content</label>
                    <div id="mainEditor"></div>
                </div>
            </div>
            <div id="pgProperties" class="tab-pane fade" role="tabpanel">
                <div class="form-check mb-2">
                    <input type="checkbox" class="form-check-input" value="bool" id="checkpoint">
                    <label class="form-check-label">Page is a checkpoint</label>
                </div>
                <div class="row">
                    <!-- Choices -->
                    <div class="col-12 col-lg-6">
                        <h4 class="border-bottom">Choices</h4>
                        <div>
                            <p>No choices yet.</p><small>You don't have to create choices, in which case the page
                                will just have a generic 'continue' button</small>
                        </div>
                        <div id="choiceList" class=mb-2></div>
                        <button class="btn btn-custom" id="choiceAdd">Add a choice</button>
                    </div>
                    <!-- Stat tools -->
                    <div class="col-12 col-lg-6">
                        <h4 class="border-bottom">Stat tools</h4>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="checkAdd">No stat checks yet, you can add them from below</label>
                                <div id="checkList"></div>
                                <div id="statInput">
                                    <select id="checkAdd" class="form-control col-auto">
                                        <!-- TODO: Koska tarkoitus on että tämä hyvin modulaarinen, nää statsit pitää pystyy muokkaan game properties tabassa, jollonka ne tulee tähän-->
                                        <option value="0" disabled selected hidden>Select a stat</option>
                                        <option value="Strength">Strength</option>
                                        <option value="Dexterity">Dexterity</option>
                                        <option value="Constitution">Constitution</option>
                                        <option value="Intelligence">Intelligence</option>
                                        <option value="Wisdom">Wisdom</option>
                                        <option value="Charisma">Charisma</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="statIncrease">Page gives stat points?</label>
                                <input class="form-control" type="number" id="statIncrease" min=0
                                    placeholder="Insert a number">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col" id=linkList>
                        <h4 class="border-bottom mt-3">Page links</h4>
                        <div class=dropdown>
                            <input type="text" class="form-control dropdown-toggle" placeholder="Search for a page"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="dropdown-menu col" aria-labelledby="dropdownMenuButton">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6" style="display:none" id="statFail">
                        <h4 class="border-bottom mt-3">Text on failure</h4>
                        <div class="form-check mb-2">
                            <input type="checkbox" class="form-check-input" value="bool">
                            <label class="form-check-label">Causes game over?</label>
                        </div>
                        <div id="failText">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="gameProperties" class="tab-pane fade" role="tabpanel">
                <p>Something</p>
            </div>
        </div>
        <div class="d-flex justify-content-between border-top mt-3">
            <button id="submit" class="btn btn-custom square-l">Save</button>
            <button class="btn btn-custom square-r" onclick="window.location.href='../';">Go to game</button>
        </div>
    </div>
    <div id="alert" class="mx-auto col-5 main container" style="display:none">
        <h1 class="text-center">Page saved successfully</h1>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="src/ckeditor/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="../bootstrap/bootstrap.min.js"></script>
<script src="src/js/idb-promise.js"></script>
<script src="src/js/indexedDB.js"></script>
<script src="src/js/editor.js"></script>
<script src="src/js/pageProperties.js"></script>
<script src="src/js/pageSel.js"></script>

</html>