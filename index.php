<?php 
include('src/php/gameData.php');
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RPG</title>
    <link rel="stylesheet" href="bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animations.css">
</head>

<body>
    <div id="loader" class="loadOut">
        <div class="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="login-form">
        <div class="main col-3 py-2" id="loginForm">
            <div class="user-login">
                <form action='javascript:void(0)'>
                <div class="form-group">
                    <label for="loginName">Username</label>
                    <input type="text" class="form-control" id="loginName">
                    <label for="loginPass">Password</label>
                    <input type="password" class="form-control" id="loginPass">
                </div>
                <p class='toggleableText'>Username or password is not correct, try again</p>
                <div class="col-12 row">
                    <input type="submit" value="Login" class="btn btn-custom" id="loginSubmit">
                </div>
                </form>
                <div class="col-12 row">
                    <a href="javascript:formSwitch();">No account? Create one!</a>
                </div>
            </div>

            <div class="user-create">
                <form action='javascript:void(0)'>
                <div class="form-group">
                    <label for="userName">Username</label>
                    <input type="text" class="form-control" id="userName">
                    <label for="userPass">Password</label>
                    <input type="password" class="form-control" id="userPass">
                    <label for="confPass">Confirm password</label>
                    <input type="password" class="form-control" id="confPass">
                </div>
                <div class="col-12 row">
                    <input type="submit" value="Create user" class="btn btn-custom" id="userSubmit">
                </div>
                </form>
                <div class="col-12 row">
                    <a href="javascript:formSwitch();">Have an account already? Login here</a>
                </div>
            </div>
            <small>Click away from the box, or press escape to hide this</small>   
        </div>
    </div>
    </div>
    <div>
        <nav class="navbar navbar-dark col-10 mx-auto px-0">
            <?php include('src/php_snippets/userFn.php')?>
            <div class='custom-tooltip'>
                <p><?php include 'src/php_snippets/userTooltip.php' ?></p>
                <button class="btn btn-custom square">Change password</button>
                <button class="btn btn-custom square">Logout</button>
            </div>
            <button class="btn btn-custom btn-nav square-r">Character</button>
        </nav>
        <div class="mx-auto row col-10 main py-4 justify-content-center content">
            <div id="pageContent" class="row col-12 justify-content-center">
                <div id="text" class="row col-12"></div>
                <div id="buttons" class="row justify-content-around col-8"></div>
            </div>
            <div id="charSheet" class="row col-12" style="display:none">
                <h4 class="col-12 mb-4">Character name here</h4>
                <span class="col-12 mb-4 row no-gutters">
                    <p>Available stat points:</p><input class="no-spinners" type="number" min=0>
                </span>
                <div class="row col-12 justify-content-center container">
                    <div class="col-12 col-sm-6 col-lg-4 d-flex justify-content-between mb-2">
                        <span class="col row justify-content-start">
                            <p class=""></p><input class="no-spinners" type="number" max="20">
                        </span>
                        <div class="col-2 btn-group justify-content-center d-flex">
                            <button class="btn btn-primary">+</button>
                            <button class="btn btn-danger">-</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 d-flex justify-content-between mb-2">
                        <span class="col row justify-content-start">
                            <p class=""></p><input class="no-spinners" type="number" max="20">
                        </span>
                        <div class="col-2 btn-group justify-content-center d-flex">
                            <button class="btn btn-primary">+</button>
                            <button class="btn btn-danger">-</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 d-flex justify-content-between mb-2">
                        <span class="col row justify-content-start">
                            <p class=""></p><input class="no-spinners" type="number" max="20" max>
                        </span>
                        <div class="col-2 btn-group justify-content-center d-flex">
                            <button class="btn btn-primary">+</button>
                            <button class="btn btn-danger">-</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 d-flex justify-content-between mb-2">
                        <span class="col row justify-content-start">
                            <p class=""></p><input class="no-spinners" type="number" max="20">
                        </span>
                        <div class="col-2 btn-group justify-content-center d-flex">
                            <button class="btn btn-primary">+</button>
                            <button class="btn btn-danger">-</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 d-flex justify-content-between mb-2">
                        <span class="col row justify-content-start">
                            <p class=""></p><input class="no-spinners" type="number" max="20">
                        </span>
                        <div class="col-2 btn-group justify-content-center d-flex">
                            <button class="btn btn-primary">+</button>
                            <button class="btn btn-danger">-</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 d-flex justify-content-between mb-2">
                        <span class="col row justify-content-start">
                            <p class=""></p><input class="no-spinners" type="number" max="20">
                        </span>
                        <div class="col-2 btn-group justify-content-center d-flex">
                            <button class="btn btn-primary">+</button>
                            <button class="btn btn-danger">-</button>
                        </div>
                    </div>
                </div>
                <button class="btn btn-custom" id='save' disabled>Save changes</button>
            </div>
        </div>
    </div>
</body>
<script src="src/js/game.js"></script>
<script src="src/js/userFn.js"></script>

</html>